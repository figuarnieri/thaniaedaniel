			<!-- Controller Player -->
			<div id="controller-player" player>
				<ul>
					<li>
						<a href="javascript:void(0)" title="Anterior" id="prev" class="fa fa-step-backward" ng-click="backward()"></a>
					</li>
					<li>
						<a href="javascript:void(0)" title="Pause" id="pause" style="display: none;" class="fa fa-pause" ng-click="pause()"></a>
					</li>
					<li>
						<a href="javascript:void(0)" title="Play" id="play" class="fa fa-play" ng-click="play()"></a>
					</li>
					<li>
						<a href="javascript:void(0)" title="Próxima" id="next" class="fa fa-step-forward" ng-click="forward()"></a>
					</li>
				</ul>
			</div>

			<!-- Player -->
			<audio id="player">
			  <source id="src-music" type="audio/mpeg">
				Your browser does not support the audio element.
			</audio>
			<!-- Aqui a diretiva não precisa ser mencionada como no menu pois a tag do html está funcionando como uma diretiva -->
			<footer role="rodape" id="footer"></footer>
		</main>

		<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="./source/js/libs/ninjax.js"></script>
		<script src="./source/js/libs/jquery.countdown.min.js"></script>

		<!-- Google Maps API -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjI5qwU0RJXCcSj594DcMatMflZbgo8Zo"></script>
		
		<!-- Utils -->
		<!-- <script type="text/javascript" src="assets/js/utils/utils.js"></script> -->

		<!-- SCRIPTS -->
		<!-- <script type="text/javascript" src="assets/js/app.js"></script> -->
		<script>
			/* Geral */
			new Ninjax({
				tag: '#menu a, .box-buttons a',
				to: '.container',
				before: function(e, f) {
						document.body.insertAdjacentHTML('beforeend','<div class="ninjax__load" />');
				},
				complete: function(e, f) {
						document.querySelectorAll('.menu__link').forEach(function(item){
							item.classList.remove('menu__link-active')
						});
						e.classList.add('menu__link-active');
						document.querySelector('.ninjax__load').remove();
				}
			});
			var modal = {
				close: function(){
					$('.modal').fadeOut();
				},
				open: function(){
					$('.modal').fadeIn();
				}
			}
			$('body').on('click', '.modal__bg', function(){
				modal.close();
			})
			$('#btn-yes').click(function(e){
				modal.close();
				$('#play').click();
			});
			$('#btn-no').click(function(e){
				modal.close();
			});
			$('#play').on('click',function(){
				var i = musics.faixaAtual;
				var _$audio = $('audio');
				var _audio = _$audio[0];
				$(this).hide();
				$('#pause').show();
				_$audio.attr('src', musics.path+musics.files[i]);
				setTimeout(function(){
					_audio.play();
					_audio.addEventListener('timeupdate', function(e){
						if(_audio.currentTime>=_audio.duration-1){
							_$audio.attr('src', musics.path+musics.files[++i]);
							_audio.play();
							musics.faixaAtual = i;
						};
					})
				},60);
			});
			$('#pause').click(function(){
				var _$audio = $('audio');
				var _audio = _$audio[0];
				$(this).hide();
				$('#play').show();
				_audio.pause();
			})
			$('#prev').click(function(){
				if(musics.faixaAtual===0){
					musics.faixaAtual = musics.files.length-1;
				} else {
					musics.faixaAtual = musics.faixaAtual-1
				}
				$('#play').click();
			});
			$('#next').click(function(){
				if(musics.faixaAtual===musics.files.length-1){
					musics.faixaAtual = 0;
				} else {
					musics.faixaAtual = musics.faixaAtual+1
				}
				$('#play').click();
			});
			$('body').on('click', '.btn-menu', function(){
				$('#box-btn-menu').toggleClass('active');
				$('#header').toggleClass('active');
				$('.overlay').toggleClass('active');
				$('body').toggleClass('body-menu-active');
			});
			if(innerWidth < 768){
				$('.dropdown > a').on('click', function(){
					$(this).toggleClass('active');
					$(this).next().toggleClass('active');
				});
				$('#menu a').on('click', function(){
					if(!$(this).parent().hasClass('dropdown')){
						$('#box-btn-menu').removeClass('active');
						$('#header').removeClass('active');
						$('.overlay').removeClass('active');
						$('body').removeClass('body-menu-active');
					}
				});
			}

			/* Home */
			var musics = {
				path: './assets/musics/',
				files: [
					'forrueiros-como_todo_amor.mp3',
					'banda_eva-eva.mp3',
					'jason_mraz-i_won_t_give_up.mp3',
					'john_legend-all_of_me.mp3',
					'mana_feat_jorge_e_matheus-voce_e_minha_religiao.mp3',
					'soja-you_and_me.mp3',
					'timbalada-minha_historia.mp3',
				],
				faixaAtual: 0
			}
			$('.countdown').countdown('2018/10/06', function(event) {
				$('.countdown-days .countdown-number').text(event.offset.totalDays)
				$('.countdown-hours .countdown-number').text(event.offset.hours)
				$('.countdown-minutes .countdown-number').text(event.offset.minutes)
				$('.countdown-seconds .countdown-number').text(event.offset.seconds)
			});
			if(localStorage.getItem('HomeOuvir')){
				if(Date.now()>parseInt(localStorage.getItem('HomeOuvir'))){
					localStorage.removeItem('HomeOuvir');
				}
			} else {
				$('.modal--home').removeClass('modal--hide');
				localStorage.setItem('HomeOuvir', Date.now()+(1000*60*60));
			}

			/* confirmar-presenca */
			$('body').on('change', '[data-confirm]', function(e){
				var _true = $(this).data('confirm');
				console.log(_true);
				if(_true==true){
					$(this).closest('.box-question').next().removeClass('box-question--hidden');
				} else {
					$(this).closest('.box-question').next().addClass('box-question--hidden');
				}
			});
			$('body').on('click', '[data-convidado]', function(e){
				var _length = $('.confirmar__line').length;
				var _pai = $(this).closest('.box-question');
				var _input = _pai.find(':text')[0];
				if(_input.value.trim().length < 3){
					return;
				}
				if(_length<10){
					_pai.append('<div class="box-companions confirmar__line confirmar__line-copy"><input class=" confirmar__insert" type="text" name="companion[]" value="'+_input.value+'" placeholder="Nome do acompanhnante"><button class="confirmar__insert confirmar__insert-button confirmar__insert-remove" type="button" data-removepresenca>-</button></div>');
				}
				if(_length===10){
					$(this).attr('disabled', true);
				} else {
					$(this).removeAttr('disabled');
					$('.confirmar__insert:eq(0)').val('');
				}
			});
			$('body').on('click', '[data-removepresenca]', function(e){
				$(this).closest('.confirmar__line').remove();
				$('[data-convidado]').removeAttr('disabled')
			});
			$('body').on('click', '[data-product]', function(){
				var _product = JSON.parse($(this).attr('data-product'));
				$('.giftmodal__image').attr({'src': _product.image,'alt': _product.name});
				$('.giftmodal__title').text(_product.name)
				$('.giftmodal__descript').text(_product.descript)
				$('.gift__brand .giftmodal__text').text(_product.brand)
				$('.gift__model .giftmodal__text').text(_product.model)
				$('.gift__color .giftmodal__text').text(_product.color)
				$('.gift__voltagem .giftmodal__text').text(_product.voltage)
				modal.open();
			});
			</script>
	</body>
	</html>
