<?php
require_once('connect.php');
$path = './';
?>
<!DOCTYPE html>
<html id="mySite" ng-app="mySite" xmlns="http://www.w3.org/1999/xhtml">
	<head>
			<title>..::Thania &amp; Daniel ::..</title>
			<link rel="icon" sizes="16x16" href="/favicon.ico">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="author" content="Ricardo Jesus <rdo_ramone@hotmail.com>">
	    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	    <link href='https://fonts.googleapis.com/css?family=Lato:100,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
      <link type="text/css" rel="stylesheet" href="/assets/css/style.min.css?<?php echo time();?>" />
	</head>
	<body>
		<div class="slide"></div>
		<div class="bg-page"></div>
		<main role="conteudo-principal">
			<header role="cabecalho" id="header">
				<nav role="menu" id="menu">
          <ul>
          <!-- Aplicar a classe "active" no link dos menus que teram dropdown -->
            <li><a href="/">Início</a></li>
            <!-- <li class="dropdown"><a href="javascript:void(0);">Sobre nós <i></i></a>
              <ul>
                <li><a href="/nossa-historia">Nossa história</a></li>
                <li><a href="/fotos">As fotos</a></li>
              </ul>
            </li> -->
            <li><a href="/fotos">As fotos</a></li>
            <li><a href="/cerimonia-de-casamento">Cerimônia de casamento</a></li>
            <!-- <li><a href="/lista-de-presentes">Presentes</a></li> -->
            <li><a href="/confirmar-presenca">RSVP</a></li>
            <!-- <li class="dropdown"><a href="javascript:void(0);">Informações <i></i></a>
              <ul>
                <li><a href="/daminhas">Daminhas</a></li>
                <li><a href="/padrinhos">Padrinhos e Madrinhas</a></li>
                <li><a href="/hospedagem">Hospedagem</a></li>
              </ul>
            </li> -->
            <li><a href="/recados">Recados</a></li>
          </ul>
        </nav>
      </header>
      <section class="menu-container">
        <header id="box-btn-menu">
          <button type="button" class="btn-menu"></button>
        </header>
      </section>
