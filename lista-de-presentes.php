<?php include('./includes/header.php');?>
  <section id="ng-view" class="container">
    <div class="wrapper" ng-click="hideMenu()">
      <div id="lista-de-presentes" class="content">
        <h2 class="title-page">Lista de Presentes</h2>
        <p class="txt">Escolha um presente e selecione qual você gostaria de nos presentear. =D</p>
        <p class="txt">Após escolher o presente, fique a vontade para compra-lo na loja de sua preferência.</p>
        <div class="container-gifts">
          <?php
          $sql = mysql_query('
            select 
              id,
              product,
              description,
              brand,
              model,
              color,
              colorhexa,
              voltage,
              amount,
              image
            from
              gifts
          ');
          while ($row = mysql_fetch_assoc($sql)) { ?>
            <div class="gifts">
              <div class="gifts__thumb">
                <img src="assets/img/gifts/<?php echo $row['image'];?>" alt="<?php echo $row['product'];?>">
              </div>
              <div class="gifts__info">
                <h2 class="gifts__name"><?php echo utf8_encode($row['product']);?></h2>
                <p class="gifts__descript"><?php echo utf8_encode($row['description']);?></p>
              </div>
              <div class="gifts__events">
                <?php if($row['amount']<1){ ?>
                  <div type="button" class="gifts__button gifts__button-disabled">Indisponível</div>
                <?php } else { ?>
                  <button type="button" class="gifts__button" data-product='{
                    "name": "<?php echo utf8_encode($row['product']);?>",
                    "descript": "<?php echo utf8_encode($row['description']);?>",
                    "brand": "<?php echo utf8_encode($row['brand']);?>",
                    "model": "<?php echo utf8_encode($row['model']);?>",
                    "color": "<?php echo utf8_encode($row['color']);?>",
                    "colorhexa": "<?php echo utf8_encode($row['colorhexa']);?>",
                    "voltage": "<?php echo utf8_encode($row['voltage']);?>",
                    "amount": <?php echo $row['amount'];?>,
                    "image": "assets/img/gifts/<?php echo utf8_encode($row['image']);?>"
                  }'>Escolher</button>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </div>
        <div class="modal modal--hide">
          <div class="modal__bg"></div>
          <div class="modal__content">
            <form class="giftmodal" name="choosenGiftForm" action="" id="form-gift" autocomplete="off">
              <div class="giftmodal__product" data-step="1">
                <div class="giftmodal__thumb">
                  <img class="giftmodal__image" src="" alt="">
                </div>
                <div class="giftmodal__info">
                  <h2 class="giftmodal__title"></h2>
                  <p class="giftmodal__descript"></p>
                  <ul class="giftmodal__details">
                    <li class="giftmodal__item gift__brand">
                      <strong class="giftmodal__name">Marca</strong>
                      <span class="giftmodal__text"></span>
                    </li>
                    <li class="giftmodal__item gift__model">
                      <strong class="giftmodal__name">Modelo</strong>
                      <span class="giftmodal__text"></span>
                    </li>
                    <li class="giftmodal__item gift__color">
                      <strong class="giftmodal__name">Cor</strong>
                      <span class="giftmodal__text"></span>
                    </li>
                    <li class="giftmodal__item gift__voltagem">
                      <strong class="giftmodal__name">Voltagem</strong>
                      <span class="giftmodal__text"></span>
                    </li>
                  </ul>
                </div>
                <div class="box-description-product">
                  <button type="button" class="btn-submit" ng-click="giftSelect()">Selecionar</button>
                </div>
              </div>
              <div class="giftmodal__identifier giftmodal--hide" data-step="2">
                <label for="guestname">Digite seu nome e e-mail abaixo:</label>
                <input type="text" id="guestname" name="guestname" placeholder="Nome" required ng-model="guestname" ng-class="{'error' : choosenGiftForm.guestname.$invalid && !choosenGiftForm.guestname.$pristine}">
                <p ng-show="choosenGiftForm.guestname.$invalid && !choosenGiftForm.guestname.$pristine" ng-class="{'txt-erro' : choosenGiftForm.guestname.$invalid && !choosenGiftForm.guestname.$pristine}">Campo obrigatório.</p>
                <input type="email" id="guestemail" name="guestemail" placeholder="E-mail" required ng-model="guestemail" ng-class="{'error' : choosenGiftForm.guestemail.$invalid && !choosenGiftForm.guestemail.$pristine}">
                <p ng-show="choosenGiftForm.guestemail.$invalid && !choosenGiftForm.guestemail.$pristine" ng-class="{'txt-erro' : choosenGiftForm.guestemail.$invalid && !choosenGiftForm.guestemail.$pristine}">Campo obrigatório.</p>
                <input type="hidden" name="giftguest" ng-model="giftguest">
                <div class="buttons">
                  <button type="button" class="btn-cancel" ng-click="cancel()">Cancelar</button>
                  <button type="button" class="btn-submit" ng-click="choosenGift(choosenGiftForm.$valid)" ng-disabled="choosenGiftForm.$invalid">Finalizar</button>
                </div>
              </div>
              <div class="giftmodal__notice giftmodal--hide"  data-step="3">
                <h3 class="giftmodal__title"></h3>
                <p class="txt">Obrigado pelo presente e por participar dessa nova etapa da nossa vida!!!</p>
                <p class="txt">Você receberá um email com o local para entrega do presente, ou se preferir, você pode nos entregar no dia do nosso casamento.</p>
                <p class="signature">Thania & Daniel</p>
              </div>
            </form>
          </div>
        </div>
        <?php include('includes/social.php')?>
      </div>
    </div>
  </section>
<?php include('includes/footer.php');?>