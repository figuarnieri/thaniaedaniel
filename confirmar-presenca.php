<?php include('includes/header.php');?>
  <section id="ng-view" class="container">
    <div class="wrapper">
      <div id="confirmar-presenca" class="content">
        <h2 class="title-page">Confirme sua presença</h2>
        <p class="txt">Preencha o formulário abaixo e confirme a sua presença, pois é muito importante para nós.</p>
        <form name="presenceForm" action="includes/confirmar-presenca-send.php" id="form-attendance-confirmation" method="POST">
          <input type="text" name="name" placeholder="Nome" required>
          <input type="email" name="email" placeholder="E-mail">
          <div class="box-question">
            <p class="txt">Você irá comparecer no casamento?</p>
            <label class="confirmar__item" for="yes">
              <input class="confirmar__item-input" id="yes" type="radio" name="presence" value="1" required data-confirm="true">
              <span class="confirmar__item-text">Sim</span>
            </label>
            <label class="confirmar__item" for="no">
              <input class="confirmar__item-input" id="no" type="radio" name="presence" value="0" required data-confirm="false">
              <span class="confirmar__item-text">Não</span>
            </label>
          </div>
          <div class="box-question box-question--hidden">
            <p class="txt">Você tem acompanhante(s) para o casamento?</p>
            <label class="confirmar__item" for="yescompanies">
              <input class="confirmar__item-input" id="yescompanies" type="radio" name="companion" value="1" data-confirm="true">
              <span class="confirmar__item-text">Sim</span>
            </label>
            <label class="confirmar__item" for="nocompanies">
              <input class="confirmar__item-input" id="nocompanies" type="radio" name="companion" value="0" data-confirm="false">
              <span class="confirmar__item-text">Não</span>
            </label>
          </div>
          <div class="box-question box-question--hidden" id="companions">
            <p class="txt">Digite abaixo o nome do seu acompanhante. Caso seja necessário adicione mais pessoas clique no <i class="btn-small add-companions">+</i> e adicione mais pessoas.</p>
            <div class="box-companions confirmar__line">
              <input class=" confirmar__insert" type="text" placeholder="Nome do acompanhnante">
              <button class=" confirmar__insert-button" type="button" data-convidado>+</button>
            </div>
          </div>
          <button type="submit" class="btn-submit" title="Confirmar">Confirmar</button>
        </form>
        <?php include('includes/social.php')?>
      </div>
    </div>
  </section>
<?php include('includes/footer.php');?>