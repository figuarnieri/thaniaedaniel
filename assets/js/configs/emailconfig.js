module.exports = {
    account: {
        gmail: {
            service: 'gmail',
            host: 'smtp.gmail.com',
            port: '587',
            secure: true,
            auth: {
              user: 'ricardoetauana@gmail.com',
              pass: ''
            }
        }
    },
    sender: {
        gmail: '"Tauana e Ricardo" <rdoramone@gmail.com>',
        hotmail: '"Tauana e Ricardo" <tauanaericardo@outlook.com>'
    }
}