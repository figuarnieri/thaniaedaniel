module.exports = {
	db_connection: {
		hml: {
			connectionLimit: 50, // Limite de conexões com o banco de dados
			host : 'localhost',
			user : 'root',
			password : '',
			database : 'ricardoetauana',
			multipleStatements: true
		},
		prod: {
			connectionLimit: 100, // Limite de conexões com o banco de dados
			host : 'mysql.ricardoetauana.com.br',
			port: 3306,
			user : '',
			password : '',
			database : 'ricardoetauana',
			multipleStatements: true
		}
	}
}