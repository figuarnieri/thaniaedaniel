<?php include('includes/header.php');?>
  <section id="ng-view" class="container">
    <div class="wrapper">
      <div id="recados" class="content">
        <h2 class="title-page">Recados</h2>
        <p class="txt">Deixe aqui um recadinho para os noivos!!!</p>
        <form action="./includes/recados-send.php" id="form-message" method="POST">
          <input type="text" name="nome" placeholder="Nome" required />
          <input type="email" name="email" placeholder="E-mail" />
          <textarea name="mensagem" cols="30" rows="5" placeholder="Digite uma messagem para o casal" required></textarea>
          <button type="submit" value="Comentar" class="btn-comment">Comentar</button>
        </form>
        <h2 class="title-page title">Veja os recados deixados para o casal</h2>
        <?php
          $sql = mysql_query('
          select 
          name,
          message,
          date
          from
          messages
          ');
          while ($row = mysql_fetch_assoc($sql)) { ?>
            <section class="box-comment">
              <div class="comment">
                <p><?php echo utf8_decode($row['message']);?></p>
                <div class="arrow-down"></div>
              </div>
              <div class="comment-author">
                <h3 class="author"><?php echo utf8_decode($row['name']);?></h3>
                <p class="date"> escrito em <?php echo $row['date'];?></p>
              </div>
            </section>
          <?php } ?>
      </div>
    </div>
  </section>
<?php include('includes/footer.php');?>