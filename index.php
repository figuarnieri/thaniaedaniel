<?php include('includes/header.php');?>
  <section id="ng-view" class="container">
    <div class="wrapper">
      <div id="home">
        <h1>Thania <i class="heart">&</i> Daniel</h1>
        <h2 class="save-the-date">06 de Outubro de 2018</h2>
        <span class="line"></span>
        <div class="box-countdown">
          <h2 class="intro-text">Faltam</h2>
          <div class="countdown">
            <div class="countdown-days">
              <p><strong class="countdown-number"></strong></p>
              <p class="countdown-title">dias</p>
            </div>
            <div class="countdown-hours">
              <p><strong class="countdown-number"></strong></p>
              <p class="countdown-title">horas</p>
            </div>
            <div class="countdown-minutes">
              <p><strong class="countdown-number"></strong></p>
              <p class="countdown-title">minutos</p>
            </div>
            <div class="countdown-seconds">
              <p><strong class="countdown-number"></strong></p>
              <p class="countdown-title">segundos</p>
            </div>
          </div>
        </div>
        <div class="box-buttons">
          <a href="/confirmar-presenca"><strong>RSVP</strong> Confirme sua presença</a>
          <a href="/lista-de-presentes"><i class="fa fa-gift icon-gift" aria-hidden="true"></i>Lista de Presentes</a>
        </div>
        <div class="box-phrase">
          <p class="phrase">Encontramos um no outro tudo aquilo que faltava no fundo de nós.</p>
        </div>
      </div>
    </div>
  </section>
  <div class="modal modal--hide modal--home">
    <div class="modal__bg"></div>
    <div class="modal__content">
      <p class="modal__title">Deseja ouvir a música do casal?</p>
      <div class="box-button">
        <button class="modal__btn modal__submit" type="button" id="btn-yes">Sim</button>
        <button class="modal__btn modal__cancel" type="button" id="btn-no">Não</button>
      </div>
    </div>
  </div>

<?php include('includes/footer.php');?>