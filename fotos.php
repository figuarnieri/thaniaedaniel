<?php include('includes/header.php');?>
  <section id="ng-view" class="container">
    <div class="wrapper" ng-click="hideMenu()">
      <div id="fotos" class="content">
        <h1 class="title-page">Nossas fotos</h1>
        <div class="gallery">
          <?php for($i=1;$i<=49;$i++){ ?>
            <a href="javascript:void(0)" title="Imagem <?php echo $i?>" ng-click="carouselDefault($event)"><img src="assets/img/couple/thumbs/<?php echo $i?>.jpg" alt="Imagem <?php echo $i?>" data-index="<?php echo $i?>" class="image square"></a>
          <?php } ?>
        </div>
        <div id="carousel" carousel ng-show="imageSelected"></div>
        <?php include('includes/social.php')?>
      </div>
    </div>
  </section>
<?php include('includes/footer.php');?>